FROM php:7.2-fpm

# Aceitar Termo Microsoft
ENV ACCEPT_EULA=Y
ENV APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=DontWarn
ENV APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=1

# set proxy
ENV http_proxy "http://10.21.84.50:3128"
ENV https_proxy  "https://10.21.84.50:3128"

# Copy composer.lock and composer.json
COPY composer.lock /var/www
COPY composer.json /var/www
COPY . /var/www/

# Set Workdir
WORKDIR /var/www

# Install dependencies
RUN apt-get update \
    && apt-get install -y \ 
    mysql-client \
    libpng-dev \
    zip \
    jpegoptim optipng pngquant gifsicle \
    gnupg2 \
    vim \
    unzip \
    git \
    curl \
    libpq-dev

RUN apt-get update \
    && apt-get install -y gnupg2 vim \
    && curl http://packages.microsoft.com/keys/microsoft.asc | apt-key add - \
    && curl http://packages.microsoft.com/config/debian/9/prod.list \
        > /etc/apt/sources.list.d/mssql-release.list \
    && apt-get install -y --no-install-recommends \
        locales \
        apt-transport-https \
    && echo "en_US.UTF-8 UTF-8" > /etc/locale.gen \
    && locale-gen \
    && apt-get update \
    && apt-get -y --no-install-recommends install \
    unixodbc-dev \
    msodbcsql17

# Clear cache
RUN rm -rf /var/lib/apt/lists/*

# Install extensions
RUN docker-php-ext-install pdo_mysql mbstring zip exif pcntl \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-install pdo pdo_pgsql pgsql

# Outra extensoes
RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
    && docker-php-ext-install -j$(nproc) iconv \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Expose port 9000 and start php-fpm server
EXPOSE 9000
CMD ["php-fpm"]
